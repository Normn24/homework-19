"use strict";

const button = document.getElementsByClassName("btn");
let currentLetter = null;

window.onload = function () {
  for (let i = 0; i < button.length; i++) {
    document.addEventListener("keyup", function (event) {
      if (event.code === button[i].textContent || event.key === button[i].textContent) {
        if (currentLetter !== null) {
          const previousButton = Array.from(button).find((button) => button.textContent === currentLetter);
          previousButton.classList.remove("blue");
        }
        button[i].classList.add("blue");
        currentLetter = button[i].textContent;
      }
    })
  }
}




